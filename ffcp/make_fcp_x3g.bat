: Hook script to run the DrLex make_fcp_x3g script under WSL
: https://gitlab.com/lscotte/3dprinting
bash ~/3d/make_fcp_x3g '%1'
: Comment the next line with : to close the window automatically
pause
