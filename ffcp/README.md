# FlashForge Creator Pro files

## WSL hook script for make_fcp_x3g

DrLex - https://www.dr-lex.be - has written a very useful
post-processing script for Slic3r for converting and sanitizing .gcode
files into the .x3g format required for FlashForge Creator Pro printers.

This hook script uses the Windows Subsystem for Linux (WSL), also
known as Bash on Ubuntu on Windows, to run the make_fcp_x3g script.
It requires files to be installed on your Windows computer, as well as
inside the WSL container. You'll need to install gpx and other programs
inside your Linux container - I'm using Ubuntu Bionic and the version of
[GPX from Ubuntu](https://packages.ubuntu.com/bionic/gpx).

### Instructions

+ Get a
  [working Linux container in WSL](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
  From a Windows CMD prompt, you need to be able to type "bash" and get a
  shell in the Linux container.
+ In the WSL container, install GPX, perl, sed, tr, and any other
  dependencies required by the make_fcp_x3g script.
+ Copy the latest version of the  make_fcp_x3g script from DrLex
  into the WSL container's filesystem. I put it in ~/3d. This script
  [is here in github](https://github.com/DrLex0/FFCP-GCodeSnippets).
  or
  [here in thingiverse](https://www.thingiverse.com/thing:2367215)
  Insure that the script is executable. **Again, this should be in
  the container's filesystem, not the Windows filesystem.**
+ Copy the [make_fcp_x3g.bat](make_fcp_x3g.bat) batch file somewhere
  in your Window's filesystem. I put it in C:\Users\myuser\3d. Note
  this script does nothing more than:

```
bash ~/3d/make_fcp_x3g '%1'
```

+ In your Slic3r configuration, add the full path to the batch file
  in the *Post-processing scripts* box, found under *Output options*
  in *Print Settings*. For example, mine is
  C:\Users\myuser\3d\make_fcp_x3g.bat.

Now, whenever you click the *Export G-code* button, Slic3r will run
the batch file, which will then run DrLex' script inside the Linux
container. The .x3g output file (as well as the .gcode backup) will
be in the same directory as the original .gcode file.

## License

As mentioned in the repository root's [README](../README.md), this
is freely available to use and distribute under a Creative Commons
and GPL license. The make_fcp_x3g script in this repository is a
modified version of DrLex work, licensed under the Creative Commons
Attribution 4.0 International license.
