# 3D Designs

This is a collection of original designs, including designs that
I have uploaded to my
[thingiverse.com collection](https://www.thingiverse.com/lscotte/designs).

These designs are available via a Creative Commons Attribution
license, as described in this repositories
[README](../README.md).
