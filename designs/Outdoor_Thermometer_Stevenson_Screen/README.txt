                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:3034323
Outdoor Thermometer Stevenson Screen by lscotte is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

I created this Stevenson Screen for our outdoor thermometer. There's plenty of similar designs on thingiverse, but I'm also learning how to create such things with Fusion 360, so it was a learning experience as well.

There are two parts to this - the main base, and a lid that sits snugly inside. There's a third .stl which contains a single louver hoop as a test print if you think this design might be useful, but don't want to wait 6+ hours just to see if your thermometer will fit inside.

There's a fair bit of support required - I included a picture at the end of the print how it looked with all the support structure still attached (I sliced via Slic3r Prusa Edition).

[Here's the Fusion 360 design](https://a360.co/2vwDhBs).

# Print Settings

Printer Brand: FlashForge
Printer: Creator Pro
Rafts: No
Supports: Yes
Resolution: .2mm
Infill: 20% Cubic