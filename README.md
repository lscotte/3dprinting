# 3D Printing Files

This repository contains a small collection of things around 3D Printing,
such as STL designs that I've created and some scripts that may be useful
to others.

The contents of this repository is here with the intent to share, and
is made available under a
[Creative Commons license](LICENSE_things)
where it applies to designs and other things, and a
[GPL v3 license](LICENSE_code)
where it applies to scripts and code.
